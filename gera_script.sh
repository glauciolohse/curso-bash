#!/usr/bin/env bash

#!/usr/bin/env bash

# [[ $# -ne 1 ]] && echo "Número incorreto de parâmtros! : $#" && exit
# [[ -d $1 ]] && echo "Isso é um diretório!" && exit
# [[ -f $1 ]] && echo "Arquivo já existe" && exit


[[ $# -ne 1 || -d $1 || -f $1 ]] && exit 1

[[ ! -f $HOME/.selected_editor ]] && select-editor
source $HOME/.selected_editor


echo '#!/usr/bin/env bash' >> $1

chmod +x $1
$SELECTED_EDITOR $1
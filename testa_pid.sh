#!/usr/bin/env bash

clear

echo "COMPARANDO PID'S DAS SESSÕES DO BASH NO SCRIPT E NO TERMINAL"
echo "------------------------------------------------------------"
echo "PID da sessão do Bash no terminal : $PPID"
echo "PID da sessão do Bash neste script: $$"
echo "------------------------------------------------------------"

# $$ -> PID do script em execução
# $PPID -> PID do pai do shell original
# pid -> ID do processo
# user -> Nome do usuário
# tty -> Terminal que o processo está vinculado
# args -> Argumentos do comando
 
ps -p $$,$PPID -o pid,user,tty,args

exit